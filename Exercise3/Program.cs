﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 5, k = 2;
            int[,] mas = new int[n, k] { { 4, -1 }, { -1, 2 }, { 0, 2 }, { 5, 4}, {6, -2 } };

            //Sum(mas, n, k);
            SaddlePoints();
            Console.WriteLine();


            Console.ReadKey();

        }

        public static void Sum(int[,] mas, int n, int k)
        {
            int sum;
            //show mas
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }


            //sum
            for (int i = 0; i < n; i++)
            {
                sum = 0;
                for (int j = 0; j < k; j++)
                {
                    if (mas[i, j] < 0)
                    {
                        for (int q = 0; q <= j; q++)
                        {
                            sum += mas[i, q];                         
                        }
                        Console.Write("sum line {0} = {1}\n", i + 1, sum);
                        Console.WriteLine();
                    }
                }
            }
        }

        public static void SaddlePoints()
        {
            const int strok = 4, stolbcov = 4;
            int i, j, k, z, maxofmin, minofmax;
            int[,] matr = new int[strok, stolbcov] { { 7, -1, -4, 1 }, { 4, 2, 3, 2 }, { 2, 2, 3, 2 }, { 4, -3, 7, -2 } };




            int[] max = new int[stolbcov];
            int[] min = new int[strok];

            for (i = 0; i < strok; i++)
            {
                Console.WriteLine();

                for (j = 0; j < stolbcov; j++)
                {
                    Console.Write("  " + matr[i, j]);
                }
            }

            j = 0;
            for (i = 0; i < strok; i++)
            {
                for (j = 0; j < stolbcov; j++)
                {
                    min[i] = matr[i,j];
                    if (min[i] > matr[i,j])
                        min[i] = matr[i,j];
                }
            }
            j = 0;
            for (j = 0; j < stolbcov; j++)
            {
                i = 0;
                max[j] = matr[i,j];
                for (i = 0; i < strok; i++)
                    if (max[j] < matr[i,j])
                        max[j] = matr[i,j];

            }
            maxofmin = min[0];
            for (i = 0; i < strok; i++)
                if (maxofmin < min[i])
                    maxofmin = min[i];
            minofmax = max[0];
            for (i = 0; i < stolbcov; i++)
                if (minofmax > max[i])
                    minofmax = max[i];
            if (minofmax > maxofmin)
                Console.Write("There are not saddle point \n");
            else
                Console.Write("\n Saddle points = \n");
            for (i = 0; i < strok; i++)
                if (min[i] == maxofmin)
                    for (j = 0; j < stolbcov; j++)
                        if (max[j] == minofmax)
                            Console.Write("{0} ({1} - {2}) \n", matr[i,j], i, j);


        }
    }
}
