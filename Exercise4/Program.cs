﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise4
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 8, k = 8;
            int[,] mas = new int[n, k];
            Random r = new Random();


            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                    mas[i,j] = r.Next(-1,2);
            }

            Sum(mas, n, k);
            Console.WriteLine();


            Console.ReadKey();
        }

        public static void Sum(int[,] mas, int n, int k)
        {
            int sum;
            //show mas
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }


            //sum
            for (int i = 0; i < n; i++)
            {
                sum = 0;
                for (int j = 0; j < k; j++)
                {
                    if (mas[i, j] < 0)
                    {
                        for (int q = 0; q <k; q++)
                        {
                            sum += mas[i, q];
                        }
                        Console.Write("sum line {0} = {1}\n", i + 1, sum);
                        Console.WriteLine();
                    }
                }
            }
        }

    }
}
