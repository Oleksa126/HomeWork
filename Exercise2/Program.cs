﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 3, k = 3;
            int[,] mas = new int[n, k] { { -1, 7, -1 }, { -1, 1, 1 }, { 0, 2, 4 } };

            First(mas, n, k);

            Console.WriteLine();
            SameElements(mas, n, k);

            Console.ReadKey();

        }

        public static void First(int [,]mas, int n, int k)
        {
            int counter = 0;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }



            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (mas[j,i] >= 0)
                    {
                        counter++;
                    }

                    if (counter == 3)
                    {
                        Console.Write("first - {0}\n", i+1);
                        }
                 
                }
                counter = 0;
            }



        }

        public static void SameElements(int[,] mas, int n, int k)
        {
            int counter = 0, nmin, p;
            int[] numberOfSameElement = new int[n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    for (int l = 0; l < k; l++)
                    {
                        //Console.Write("{0} - {1}", mas[i, l], mas[i, j]);
                        //Console.WriteLine();
                        if (mas[i, l] == mas[i, j])
                        {
                            counter++;
                        }
                    }

                }
                Console.Write("counter - {0}\n", counter);
                numberOfSameElement[i] = counter;
                counter = 0;
            }



            for (int i = 0; i < n - 1; i++)
            {
                nmin = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (numberOfSameElement[j] < numberOfSameElement[nmin])
                    {

                        nmin = j;
                        p = numberOfSameElement[i];
                        numberOfSameElement[i] = numberOfSameElement[nmin];
                        numberOfSameElement[nmin] = p;
                        for (int f = 0; f < k; f++)
                        {
                            Console.Write(mas[nmin, f] + " ");
                        } 
                        Console.WriteLine();
                    }
                }
            }

            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write(numberOfSameElement[i] + " ");
            //}
        }

    }
}
