﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Author
    {
        private String firstName;
        private String lastName;

        public Author ()
        {
        }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;

        }

        public string FirstName {
            get { return this.firstName; }
            set { firstName = value; }
        }

        public string LastName {
            get { return this.lastName; }
            set { lastName = value; }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            return base.ToString();
        }
    }
}
