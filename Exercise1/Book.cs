﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Book
    {
        private string name;
        private int pages;
        private Author author;

        public Book()
        {

        }

        public string Name
        {
            get { return this.name; }
            set { name = value; }
        }

        public int Pages
        {
            get { return this.pages; }
            set { pages = value; }
        }


    }
}
