﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Department
    {
        private string name;

        public Department()
        {

        }

        public Department(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }


}
